﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

using ChatAppSPA.Models;

namespace ChatAppSPA.Hubs
{
    public class ChatHub : Hub
    {
        static List<ChatHabUser> Users = new List<ChatHabUser>();

        // Отправка сообщений
        public void SendingMessage(int id, MessageViewModel message)
        {
            var connId = Context.ConnectionId;
            Clients.AllExcept(connId).onNewMessage(id, message);
        }

        // Подключение нового пользователя
        public void Connect(int id)
        {
            var connId = Context.ConnectionId;


            if (!Users.Any(x => x.ConnectionId == connId))
            {
                Users.Add(new ChatHabUser { ConnectionId = connId, UserId = id });

                // Посылаем сообщение текущему пользователю
                Clients.Caller.onConnected(connId, Users);

                // Посылаем сообщение всем пользователям, кроме текущего
                Clients.AllExcept(connId).onNewUserConnected(connId, id);
            }
        }

        // Отключение пользователя
        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            var item = Users.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                Users.Remove(item);
                var connId = Context.ConnectionId;
                Clients.All.onUserDisconnected(connId, item.UserId);
            }

            return base.OnDisconnected(stopCalled);
        }
    }
}
