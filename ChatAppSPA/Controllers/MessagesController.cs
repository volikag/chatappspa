﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Diagnostics;
using Microsoft.AspNet.Identity;

using ChatAppSPA.Models;
using ChatAppSPA.Models.Entities;
using ChatAppSPA.Models.Repositories;

namespace ChatAppSPA.Controllers
{
    [Authorize]
    public class MessagesController : ApiController
    {
        private IChatUserRepository usersRepo;
        private IChatRoomRepository roomsRepo;
        private IMessageRepository messagesRepo;

        public MessagesController(IChatUserRepository usersRepo, IChatRoomRepository roomsRepo, IMessageRepository messagesRepo)
        {
            this.usersRepo = usersRepo;
            this.roomsRepo = roomsRepo;
            this.messagesRepo = messagesRepo;
        }

        // GET: api/Messages/5
        [ResponseType(typeof(IEnumerable<Message>))]
        public async Task<IHttpActionResult> GetMessages(int chatId, int userId)
        {
            return await GetMessages(chatId, userId, -1);
        }

        // GET: api/Messages/5
        [ResponseType(typeof(IEnumerable<Message>))]
        public async Task<IHttpActionResult> GetMessages(int chatId, int userId, int from)
        {
            var cuId = User.Identity.GetUserId();

            var currentUser = await usersRepo.GetByAppUserIdAsync(cuId);


            if (chatId == 0)
            {
                return Ok(new Message[0]);
            }

            ChatRoom chatRoom = roomsRepo.GetChatRoomById(chatId);

            if (chatRoom == null)
            {
                return NotFound();
            }

            bool canView = roomsRepo.CanViewChatRoomData(chatRoom, currentUser);

            if (!canView)
            {
                return BadRequest("You can not view messages from this chat");
            }

            var messages = await messagesRepo.GetMessagesForChatRoomAsync(chatRoom, from);

            if (messages == null)
            {
                return Ok(new Message[0]);
            }

            return Ok(messages);
        }


        // POST: api/Messages
        [ResponseType(typeof(MessageViewModel))]
        public async Task<IHttpActionResult> PostMessage(MessageViewModel message)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userId = User.Identity.GetUserId();

            var author = await usersRepo.GetByAppUserIdAsync(userId);

            ChatRoom chatRoom = null;

            if (message.ChatRoomId != 0)
            {
                int id = message.ChatRoomId;

                chatRoom = roomsRepo.GetChatRoomById(id);
            }
            else
            {
                if (message.InPersonalChatRoom)
                {
                    int id = message.AddresseeId;

                    IEnumerable<ChatRoom> chatRooms = roomsRepo.GetPersonalCommonChatRoom(id, author.Id);

                    if (chatRooms.Count() == 0)
                    {
                        var otherUser = usersRepo.GetById(id);

                        chatRoom = roomsRepo.CreateNewPersonalChatRoomForUsers(author, otherUser);
                    }
                    else
                    {
                        chatRoom = chatRooms.First();
                    }
                }
                else
                {
                    //Error: all public chats must be accessable
                }
            }

            bool canView = roomsRepo.CanViewChatRoomData(chatRoom, author);

            if (!canView)
            {
                return BadRequest("You can not add messages to this chat");
            }

            Message msg = messagesRepo.AddNewMessage(chatRoom, author, message.Text, message.Date);

            // Update message Id for appending on frontend
            message.Id = msg.Id;
            // Update chatRoom if if it was created
            if (message.ChatRoomId == 0)
            {
                message.ChatRoomId = chatRoom.Id;
            }

            await roomsRepo.UpdateChatRoomLastChangedDateAsync(chatRoom, message.Date);

            return CreatedAtRoute("DefaultApi", new { id = msg.Id }, message);
        }
    }
}