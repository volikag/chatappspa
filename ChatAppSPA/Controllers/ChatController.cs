﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;

using ChatAppSPA.Models;
using ChatAppSPA.Models.Entities;
using ChatAppSPA.Models.Repositories;

namespace ChatAppSPA.Controllers
{
    [Authorize]
    public class ChatController : ApiController
    {

        private IChatUserRepository usersRepo;
        private IChatRoomRepository roomsRepo;

        public ChatController(IChatUserRepository users, IChatRoomRepository rooms)
        {
            this.usersRepo = users;
            this.roomsRepo = rooms;
        }

        [ResponseType(typeof(ChatUser))]
        public async Task<IHttpActionResult> RetrieveCurrentUserData()
        {
            var userId = User.Identity.GetUserId();

            ChatUser chatUser = await usersRepo.GetByAppUserIdAsync(userId);

            //Create new ChatUser if not exists
            if (chatUser == null)
            {
                chatUser = usersRepo.CreateNewChatUserForAppUser(userId);
                await roomsRepo.AddUserToPublicRoomsAsync(chatUser);
            }

            if (chatUser == null)
            {
                return this.NotFound();
            }

            return this.Ok(chatUser);
        }

        public IEnumerable<ChatUser> GetUsersList()
        {
            return usersRepo.GetAllUsers();
        }

        public IEnumerable<ChatRoomViewModel> GetChatRoomsList()
        {
            List<ChatRoomViewModel> chatRooms = new List<ChatRoomViewModel>();

            var userId = User.Identity.GetUserId();
            var currentUser = usersRepo.GetByAppUserId(userId);

            // First get all public chat rooms
            IEnumerable<ChatRoom> publicChatRooms = roomsRepo.GetAllPublicChatRooms();

            foreach (var chat in publicChatRooms)
            {
                chatRooms.Add(new ChatRoomViewModel() { Id = chat.Id, Name = chat.Name, IsPersonalChatRoom = false });
            }

            // Than add all exiasted personal chat rooms
            IEnumerable<ChatUsersInRoom> personalChatRooms = roomsRepo.GetAllPersonalChatRoomsForUserIncludingRooms(currentUser);
            Dictionary<int, ChatUser> allUsers = usersRepo.GetAllUsers().ToDictionary(u => u.Id);

            foreach (var ur in personalChatRooms)
            {
                var user = roomsRepo.GetPartnerForPersonalChatRoom(ur.ChatRoom, currentUser);
                allUsers.Remove(user.Id);
                chatRooms.Add(new ChatRoomViewModel() { Id = ur.ChatRoomId, ChatUserId = user.Id, Name = user.Nick, IsPersonalChatRoom = true });
            }

            // And finaly get all users to show unexisted personal chats except current user
            foreach (var user in allUsers)
            {
                if (user.Value != currentUser)
                {
                    chatRooms.Add(new ChatRoomViewModel() { Id = 0, ChatUserId = user.Value.Id, Name = user.Value.Nick, IsPersonalChatRoom = true });
                }
            }

            return chatRooms;
        }

    }
}
