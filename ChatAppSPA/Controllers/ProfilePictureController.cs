﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ChatAppSPA.Models;
using ChatAppSPA.Models.Repositories;

namespace ChatAppSPA.Controllers
{
    public class ProfilePictureController : Controller
    {
        private IChatUserRepository user;

        public ProfilePictureController(IChatUserRepository user)
        {
            this.user = user;
        }

        public ActionResult Show(int id)
        {
            byte[] imageData = user.GetProfilePicture(id, Server.MapPath("~/Content/default_avatar.png"));

            return new FileStreamResult(new System.IO.MemoryStream(imageData), "image/jpeg");
        }
    }
}