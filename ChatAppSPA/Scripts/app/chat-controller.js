﻿app = angular.module('ChatAppSPA', []);

app.directive("scroll", function ($window) {
    return function (scope, element, attrs) {
        angular.element(element).bind("scroll", function () {
            scope.scrollChanged(this.scrollTop);
            scope.$apply();
        });
    };
});

app.run(['$anchorScroll', function ($anchorScroll) {
    $anchorScroll.yOffset = 50;   // always scroll by 50 extra pixels
}]);

app.controller('ChatCtrl', ['$anchorScroll', '$location', '$scope', '$http',
  function ($anchorScroll, $location, $scope, $http) {
      $scope.title = "starting app...";
      $scope.successResult = true;

      $scope.working = false;
      $scope.retrieving = false;
      $scope.moreMessages = 0;

      $scope.firstMessageId = 0;
      $scope.lastMessageId = 0;
      $scope.serverHasMoreMessages = true;

      $scope.connectionId = null;
      $scope.currentUser = null;
      $scope.selectedChat = null;
      $scope.chatRooms = [];
      $scope.users = [];
      $scope.messages = [];

      // Ссылка на автоматически-сгенерированный прокси хаба
      $scope.chatHub = $.connection.chatHub;

      // Функция, вызываемая при подключении нового пользователя
      $scope.chatHub.client.onConnected = function (connId, allUsers) {
          console.debug("onConnected");
          $scope.connectionId = connId;

          if ($scope.currentUser !== undefined) {
              $scope.currentUser.isOnline = true;
          }

          // Добавление всех пользователей
          for (i = 0; i < allUsers.length; i++) {
              var chat = $scope.chatRooms.find(x => x.chatUserId === allUsers[i].userId);
              if (chat !== undefined) {
                  chat.isOnline = true;
              }

              //console.debug(user);
          }
          $scope.$apply();
      };

      // Добавляем нового пользователя
      $scope.chatHub.client.onNewUserConnected = function (connId, id) {
          console.debug("onNewUserConnected");

          var chat = $scope.chatRooms.find(x => x.chatUserId === id);

          if (chat !== undefined) {
              chat.isOnline = true;
          }
          else {
              //update users list
              $scope.getUsersList();
              $scope.getChatRoomList();
              //console.debug("refresh UL");
          }

          //console.debug(chat);

          $scope.$apply();
      };

      // Удаляем пользователя
      $scope.chatHub.client.onUserDisconnected = function (connId, id) {
          console.debug("onUserDisconnected");

          var chat = $scope.chatRooms.find(x => x.chatUserId === id);
          if (chat !== undefined) {
              chat.isOnline = false;
          }
          //console.debug(chat);

          $scope.$apply();
      };

      // Объявление функции, которая хаб вызывает при получении сообщений
      $scope.chatHub.client.onNewMessage = function (id, message) {
          console.debug("addMessage");

          //console.debug(message);

          var chat = $scope.chatRooms.find(x => x.id === message.chatRoomId);

          //console.debug(chat);

          if (chat === undefined) {
              // Try to fild chat by user
              if (message.inPersonalChatRoom) {
                  if ($scope.currentUser.id === message.addresseeId) {
                      // May be chat was created by other user
                      chat = $scope.chatRooms.find(x => x.chatUserId === message.addresseeId || x.chatUserId === message.authorId);

                      //console.debug(chat);

                      if (chat !== undefined) {
                          // so need to update chatList by adding 
                          chat.id = message.chatRoomId;
                      }
                  }
              }
          }

          //console.debug(chat);

          if (chat !== undefined) {
              chat.isOnline = true;
              chat.newMessages += 1;

              $scope.reorderChat(chat);

              if ($scope.selectedChat !== null) {
                  if ($scope.selectedChat === chat) {
                      $scope.addMessage(message);
                  }
              }
          }

          $scope.$apply();
      };


      // Открываем соединение
      $scope.openHubConnection = function () {
          $.connection.hub.start().done(function () {
              console.debug("openHubConnection");

              $scope.chatHub.server.connect($scope.currentUser.id);
          });
      };

      $scope.reorderChat = function (chat) {
          // Reorder chats
          console.debug(chat);
          var index = $scope.chatRooms.indexOf(chat);

          console.debug(index);
          if (index > -1) {
              $scope.chatRooms.splice(index, 1);
          }

          $scope.chatRooms.unshift(chat)
      };

      $scope.getImagePath = function (id) {
          if (Number.isInteger(id)) {
              return "/ProfilePicture/Show/" + id;
          }
          else {
              return "/Content/default_avatar.png";
          }
      };

      $scope.retrieveCurrentUserData = function () {
          $scope.working = true;
          $scope.title = "register user...";
          $scope.chatRooms = [];
          $scope.users = [];

          $http.post("/api/chat/retrieveCurrentUserData")
          .then(function (response) {
              //First function handles success
              $scope.successResult = true;

              $scope.currentUser = response.data;

              $scope.title = "user registered...";
              $scope.working = false;

              $scope.openHubConnection();

              $scope.getUsersList();

              $scope.getChatRoomList();

          }, function (response) {
              //Second function handles error
              $scope.successResult = false;

              $scope.title = "Oops... something went wrong";
              $scope.working = false;
          });
      };

      $scope.getUsersList = function () {
          $scope.working = true;
          $scope.title = "loading users...";
          $scope.users = [];

          $http.get("/api/chat/getUsersList")
          .then(function (response) {
              //First function handles success
              $scope.successResult = true;

              //console.debug(response);

              for (var i in response.data) {

                  var user = response.data[i];

                  user.imagePath = $scope.getImagePath(user.id);
                  $scope.users.push(user);
              }

              $scope.title = "users loaded...";
              $scope.working = false;

          }, function (response) {
              //Second function handles error
              $scope.successResult = false;

              $scope.title = "Oops... something went wrong";
              $scope.working = false;
          });
      };

      $scope.getChatRoomList = function () {
          $scope.working = true;
          $scope.title = "getting chatrooms...";
          $scope.chatRooms = [];

          $http.get("/api/chat/getChatRoomsList")
          .then(function (response) {
              //First function handles success
              $scope.successResult = true;

              //console.debug(response);
              for (var i in response.data) {

                  var chat = response.data[i];

                  chat.isOnline = false;
                  chat.newMessages = 0;

                  if (chat.isPersonalChatRoom) {
                      chat.imagePath = $scope.getImagePath(chat.chatUserId);
                  }
                  else {
                      chat.imagePath = $scope.getImagePath();
                  }

                  $scope.chatRooms.push(chat);

              }

              $scope.title = "chatrooms received...";
              $scope.working = false;

          }, function (response) {
              //Second function handles error
              $scope.successResult = false;

              $scope.title = "Oops... something went wrong";
              $scope.working = false;
          });
      };

      $scope.isOnline = function (chat) {
          return chat.isOnline === true;
      };

      $scope.isActive = function (chat) {
          return $scope.selectedChat === chat;
      };

      $scope.isOwnerMessage = function (user) {
          return $scope.currentUser.id === user.id;
      };
      $scope.isOtherMessage = function (chat) {
          if ($scope.selectedChat !== undefined) {
              return $scope.selectedChat.id === chat.id;
          }
          else {
              return false;
          }
      };


      $scope.selectChat = function (chat) {
          $scope.messages = [];

          if (chat === $scope.selectedChat) {
              $scope.selectedChat = null;
              $scope.lastMessageId = 0;
          }
          else {
              $scope.selectedChat = chat;
              $scope.getMessages(chat);
              //$scope.gotoLastMessage();
          }
      };

      $scope.gotoLastMessage = function () {
          //console.debug('GLM'+$scope.lastMessageId);
          $scope.gotoAnchor($scope.lastMessageId);
          $scope.selectedChat.newMessages = 0;
      };

      $scope.gotoAnchor = function (x) {
          var newHash = 'anchor' + x;
          if ($location.hash() !== newHash) {
              // set the $location.hash to `newHash` and
              // $anchorScroll will automatically scroll to it
              $location.hash('anchor' + x);
              $scope.moreMessages = 0;
          } else {
              // call $anchorScroll() explicitly,
              // since $location.hash hasn't changed
              $anchorScroll();
          }
      };

      $scope.scrollChanged = function (pos) {
          //console.debug($scope.retrieving);
          if (!$scope.retrieving) {
              $scope.retrieving = true;
              if (pos === 0) {
                  //Retrive more messages
                  if ($scope.serverHasMoreMessages) {
                      var oldpos = $scope.firstMessageId;
                      $scope.retrieveMessagesBefore($scope.selectedChat, oldpos);
                      ////console.debug('OP'+oldpos);
                      ////$scope.gotoAnchor(oldpos);
                  }
              }
              $scope.retrieving = false;
          }
      };
      $scope.appendMessages = function (data) {
          $scope.lastMessageId = 0;

          $scope.serverHasMoreMessages = false;

          for (var m in data) {
              var msg = data[m];

              msg.author = $scope.users.find(x => x.id === msg.authorId);

              $scope.messages.unshift(msg);

              if ($scope.lastMessageId === 0) {
                  $scope.lastMessageId = msg.id;
              }

              $scope.firstMessageId = msg.id;

              $scope.serverHasMoreMessages = true;
          }
          //console.debug('ALM' + $scope.lastMessageId);
      };

      $scope.retrieveMessagesBefore = function (chat, from) {
          $scope.retrieving = true;
          console.debug('retrieveMessagesBefore' + $scope.lastMessageId);

          var apiPath = '/api/messages/' + chat.id + '/' + chat.chatUserId + '/' + from;

          $scope.getMessagesReqest(apiPath);
      };

      $scope.getMessages = function (chat) {
          $scope.retrieving = true;

          console.debug('getMessages' + $scope.lastMessageId);

          var apiPath = '/api/messages/' + chat.id + '/' + chat.chatUserId;

          $scope.messages = [];

          $scope.getMessagesReqest(apiPath);
      };

      $scope.getMessagesReqest = function (apiPath) {
          $scope.working = true;
          $scope.title = "getting messages...";

          $http.get(apiPath)
          .then(function (response) {
              //First function handles success
              $scope.successResult = true;

              $scope.appendMessages(response.data);

              $scope.title = "messages get succcess...";

              $scope.working = false;

              $scope.gotoLastMessage();
              //console.debug('GMRLM' + $scope.lastMessageId);

              $scope.retrieving = false;
          }, function (response) {
              //Second function handles error
              $scope.successResult = true;
              $scope.title = "Oops... something went wrong";
              $scope.working = false;
          });
      };

      $scope.addMessage = function (message) {

          message.author = $scope.users.find(x => x.id === message.authorId);

          //console.debug(message.addresseeId);

          $scope.lastMessageId = message.id;
          $scope.messages.push(message);
          $scope.moreMessages++;

          $scope.gotoLastMessage();
      };

      $scope.sendMessage = function () {
          $scope.working = true;

          $scope.title = "sending message...";

          //console.debug($scope.selectedChat);
          var msg = {
              addresseeId: $scope.selectedChat.chatUserId,
              chatRoomId: $scope.selectedChat.id,
              inPersonalChatRoom:$scope.selectedChat.isPersonalChatRoom,
              authorId: $scope.currentUser.id,
              text: $scope.messageText,
              date: new Date().toISOString()
          };
          //console.debug(msg);

          var chat = $scope.selectedChat;

          $http.post('/api/messages',
              msg,
                  { "Content-Type": "application/json" })
          .then(function (response) {
              //First function handles success
              $scope.successResult = true;

              //console.debug(response.data);
              var result = response.data;
              $scope.chatHub.server.sendingMessage($scope.currentUser.id, result);

              if (chat.id === 0)
              {
                  chat.id = result.chatRoomId;
              }

              $scope.reorderChat(chat);

              $scope.addMessage(result);

              $scope.title = "message send success...";
              $scope.working = false;

              $scope.messageText = "";

          }, function (response) {
              //Second function handles error
              $scope.successResult = false;

              $scope.title = "Oops... something went wrong";
              $scope.working = false;
          });
      };
  }]);