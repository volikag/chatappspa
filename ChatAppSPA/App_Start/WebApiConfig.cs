﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Routing.Constraints;

using Newtonsoft.Json.Serialization;


namespace ChatAppSPA
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Use camel case for JSON data.
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "ActionsThreeParamRoute",
                routeTemplate: "api/{controller}/{chatId}/{userId}/{from}",
                defaults: new { },
                constraints: new
                {
                    chatId = new IntRouteConstraint(),
                    userId = new IntRouteConstraint(),
                    from = new IntRouteConstraint(),
                }
            );

            config.Routes.MapHttpRoute(
                name: "ActionsTwoParamRoute",
                routeTemplate: "api/{controller}/{chatId}/{userId}",
                defaults: new { },
                constraints: new
                {
                    chatId = new IntRouteConstraint(),
                    userId = new IntRouteConstraint(),
                }
            );

            config.Routes.MapHttpRoute(
                name: "ActionsRoute",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { },
                constraints: new
                {
                    action = new AlphaRouteConstraint()
                }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApiController",
                routeTemplate: "api/{controller}"
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional },
                constraints: new
                {
                    id = new IntRouteConstraint()
                }
            );
        }
    }
}
