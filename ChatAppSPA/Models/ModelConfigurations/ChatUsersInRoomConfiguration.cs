﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

using ChatAppSPA.Models.Entities;

namespace ChatAppSPA.Models.ModelConfigurations
{
    public class ChatUsersInRoomConfiguration : EntityTypeConfiguration<ChatUsersInRoom>
    {
        public ChatUsersInRoomConfiguration()
        {
            this.HasKey(ur => ur.Id);

            this.Property(ur => ur.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.HasRequired(ur => ur.ChatRoom)
                .WithMany(r => r.UsersInRoom);

            this.HasRequired(ur => ur.ChatUser)
                .WithMany(u => u.ParticipateInRooms);
        }
    }
}