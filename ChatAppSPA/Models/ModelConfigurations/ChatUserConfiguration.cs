﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

using ChatAppSPA.Models.Entities;

namespace ChatAppSPA.Models.ModelConfigurations
{
    public class ChatUserConfiguration : EntityTypeConfiguration<ChatUser>
    {
        public ChatUserConfiguration()
        {
            this.HasKey(u => u.Id);

            this.Property(u => u.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.HasRequired(u => u.ApplicationUser)
                .WithOptional(u => u.ChatUser)
                .Map(p => p.MapKey("UserId"));

            this.Property(u => u.Nick)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(u => u.IsDeleted)
                .IsRequired();

            this.HasMany(u => u.PostedMessages)
                .WithRequired(m => m.Author);

            this.HasMany(u => u.ParticipateInRooms)
                .WithRequired(m => m.ChatUser);
        }
    }
}