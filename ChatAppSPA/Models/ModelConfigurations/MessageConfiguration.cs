﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

using ChatAppSPA.Models.Entities;

namespace ChatAppSPA.Models.ModelConfigurations
{
    public class MessageConfiguration : EntityTypeConfiguration<Message>
    {
        public MessageConfiguration()
        {
            this.HasKey(m => m.Id);

            this.Property(m => m.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.HasRequired(m => m.Author)
                .WithMany(u => u.PostedMessages);

            this.HasRequired(m => m.ChatRoom)
                .WithMany(r => r.Messages);
                //.WillCascadeOnDelete(false);
        }
    }
}