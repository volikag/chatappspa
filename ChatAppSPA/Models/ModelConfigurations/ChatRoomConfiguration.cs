﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

using ChatAppSPA.Models.Entities;

namespace ChatAppSPA.Models.ModelConfigurations
{
    public class ChatRoomConfiguration : EntityTypeConfiguration<ChatRoom>
    {
        public ChatRoomConfiguration()
        {
            this.HasKey(r => r.Id);

            this.Property(r => r.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(r => r.Name).IsOptional();

            this.Property(r => r.IsPersonalChatRoom).IsRequired();
            this.Property(r => r.LastChanged).IsRequired();

            this.HasMany(r => r.Messages)
                .WithRequired(m => m.ChatRoom);

            this.HasMany(r => r.UsersInRoom)
                .WithRequired(u => u.ChatRoom);
        }
    }
}