﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChatAppSPA.Models.Entities;
using System.Data.Entity;

namespace ChatAppSPA.Models.Repositories
{
    public class ChatUserRespository : IChatUserRepository
    {
        private IApplicationDbContext dbc;
        public ChatUserRespository(IApplicationDbContext dbc)
        {
            this.dbc = dbc;
        }

        public ChatUser GetByAppUserId(string id)
        {
            var user = dbc.Users.Where(u => u.Id == id).FirstOrDefault();
            //var user = db.ChatUsers.Where(u => u.ApplicationUser.Id == userId).First();
            return user.ChatUser;
        }


        public async System.Threading.Tasks.Task<ChatUser> GetByAppUserIdAsync(string id)
        {
            var user = await dbc.Users.Where(u => u.Id == id).FirstAsync();
            return user.ChatUser;
        }

        public ChatUser GetById(int id)
        {
            return dbc.ChatUsers.Where(u => !u.IsDeleted && u.Id == id).FirstOrDefault();
        }

        public IEnumerable<ChatUser> GetAllUsers()
        {
            return dbc.ChatUsers.Where(u => !u.IsDeleted);
        }

        public byte[] GetProfilePicture(int id, string defaultPicturePath)
        {
            var pic = dbc.Users.Where(u => u.ChatUser.Id == id).First().ProfilePicture;

            byte[] imageData = null;

            if (pic != null)
            {
                imageData = pic.ToArray();
            }
            else
            {
                //Default avatar
                imageData = System.IO.File.ReadAllBytes(defaultPicturePath);
            }

            return imageData;
        }

        public ApplicationUser GetAppUserByAppUserId(string id)
        {
            return dbc.Users.Where(u => u.Id == id).FirstOrDefault();
        }

        public ChatUser CreateNewChatUserForAppUser(string appUserId)
        {
            ApplicationUser user = GetAppUserByAppUserId(appUserId);

            ChatUser chatUser = null;

            if (user != null)
            {
                chatUser = new ChatUser() { ApplicationUser = user, IsDeleted = false, Nick = user.UserName };

                dbc.ChatUsers.Add(chatUser);
                dbc.SaveChanges();
            }

            return chatUser;
        }
    }
}