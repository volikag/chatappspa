﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ChatAppSPA.Models.Entities;

namespace ChatAppSPA.Models.Repositories
{
    public interface IChatUserRepository
    {
        ChatUser CreateNewChatUserForAppUser(string appUserId);

        ChatUser GetByAppUserId(string id);
        Task<ChatUser> GetByAppUserIdAsync(string id);

        ChatUser GetById(int id);

        IEnumerable<ChatUser> GetAllUsers();

        byte[] GetProfilePicture(int id, string defaultPicturePath);
    }
}