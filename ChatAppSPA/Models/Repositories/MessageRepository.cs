﻿using ChatAppSPA.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ChatAppSPA.Models.Repositories
{
    public class MessageRepository : IMessageRepository
    {
        private IApplicationDbContext dbc;
        public MessageRepository(IApplicationDbContext dbc)
        {
            this.dbc = dbc;
        }

        /// <summary>
        /// Returns async @count messages for given ChatRoom
        /// ordered by date desc
        /// ended with @lastMessageId
        /// </summary>
        /// <param name="chatRoom">ChatRoom with messages</param>
        /// <param name="lastMessageId">Last message id for given messages</param>
        /// <param name="count">Number of messages to retrive</param>
        /// <returns></returns>
        public async Task<IEnumerable<Message>> GetMessagesForChatRoomAsync(ChatRoom chatRoom, int lastMessageId, int count = 10)
        {
            return await dbc.Messages.Where(m => (m.ChatRoomId == chatRoom.Id) && ((lastMessageId < 0) || (m.Id < lastMessageId)))
                                                    .OrderByDescending(m => m.Date)
                                                    .Take(count)
                                                    .Include(u => u.Author).ToListAsync();
        }

        public async Task<Message> AddNewMessageAsync(ChatRoom chatRoom, ChatUser author, string text, DateTime date)
        {
            var msg = new Message() { Author = author, ChatRoom = chatRoom, Date = date, Text = text };

            dbc.Messages.Add(msg);

            await dbc.SaveChangesAsync();

            return msg;
        }

        public Message AddNewMessage(ChatRoom chatRoom, ChatUser author, string text, DateTime date)
        {
            var msg = new Message() { Author = author, ChatRoom = chatRoom, Date = date, Text = text };

            try
            {
                dbc.Messages.Add(msg);

                dbc.SaveChanges();
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex);
            }

            return msg;
        }

    }
}