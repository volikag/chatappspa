﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

using ChatAppSPA.Models.Entities;
using ChatAppSPA.Models.ModelConfigurations;
using System.Threading;
using System.Threading.Tasks;

namespace ChatAppSPA.Models.Repositories
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>()
                .HasOptional(m => m.ChatUser)
                .WithRequired(m => m.ApplicationUser)
                .Map(p => p.MapKey("UserId"));

            modelBuilder.Entity<ApplicationUser>()
                .Property(au => au.ProfilePicture)
                .HasColumnType("image");

            modelBuilder.Configurations.Add(new ChatRoomConfiguration());
            modelBuilder.Configurations.Add(new ChatUserConfiguration());
            modelBuilder.Configurations.Add(new ChatUsersInRoomConfiguration());
            modelBuilder.Configurations.Add(new MessageConfiguration());
        }


        public IDbSet<ChatUser> ChatUsers { get; set; }
        public IDbSet<ChatRoom> ChatRooms { get; set; }
        public IDbSet<Message> Messages { get; set; }
        public IDbSet<ChatUsersInRoom> ChatUsersInRooms { get; set; }


        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return base.SaveChangesAsync(cancellationToken);
        }
    }
}