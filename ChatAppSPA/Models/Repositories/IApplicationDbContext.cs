﻿using System;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;

using ChatAppSPA.Models.Entities;


namespace ChatAppSPA.Models.Repositories
{
    public interface IApplicationDbContext : IDisposable
    {
        IDbSet<ChatRoom> ChatRooms { get; set; }
        IDbSet<ChatUser> ChatUsers { get; set; }
        IDbSet<ChatUsersInRoom> ChatUsersInRooms { get; set; }
        IDbSet<Message> Messages { get; set; }
        IDbSet<ApplicationUser> Users { get; set; }

        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}