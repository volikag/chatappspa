﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

using ChatAppSPA.Models.Entities;

namespace ChatAppSPA.Models.Repositories
{
    public interface IChatRoomRepository
    {
        IEnumerable<ChatRoom> GetAllPublicChatRooms();
        IEnumerable<ChatUsersInRoom> GetAllPersonalChatRoomsForUserIncludingRooms(ChatUser currentUser);

        IEnumerable<ChatRoom> GetPersonalCommonChatRoom(int firstUserId, int secondUserId);

        ChatUser GetPartnerForPersonalChatRoom(ChatRoom chat, ChatUser user);

        ChatRoom CreateNewPersonalChatRoomForUsers(ChatUser firstUser, ChatUser secondUser);

        ChatRoom GetChatRoomById(int id);

        bool CanViewChatRoomData(ChatRoom chat, ChatUser user);

        void AddUserToPublicRooms(ChatUser chatUser);
        Task AddUserToPublicRoomsAsync(ChatUser chatUser);
        void UpdateChatRoomLastChangedDate(ChatRoom chatRoom, DateTime date);
        Task UpdateChatRoomLastChangedDateAsync(ChatRoom chatRoom, DateTime date);
    }
}