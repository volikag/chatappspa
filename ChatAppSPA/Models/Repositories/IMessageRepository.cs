﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

using ChatAppSPA.Models.Entities;


namespace ChatAppSPA.Models.Repositories
{
    public interface IMessageRepository
    {
        /// <summary>
        /// Returns async @count messages for given ChatRoom
        /// ordered by date desc
        /// ended with @lastMessageId
        /// </summary>
        /// <param name="chatRoom">ChatRoom with messages</param>
        /// <param name="lastMessageId">Last message id for given messages</param>
        /// <param name="count">Number of messages to retrive</param>
        /// <returns></returns>
        Task<IEnumerable<Message>> GetMessagesForChatRoomAsync(ChatRoom chatRoom, int lastMessageId, int count = 10);

        Task<Message> AddNewMessageAsync(ChatRoom chatRoom, ChatUser author, string text, DateTime date);
        Message AddNewMessage(ChatRoom chatRoom, ChatUser author, string text, DateTime date);
    }
}