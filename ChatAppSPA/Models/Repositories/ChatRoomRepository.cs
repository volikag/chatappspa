﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

using ChatAppSPA.Models.Entities;

namespace ChatAppSPA.Models.Repositories
{
    public class ChatRoomRepository : IChatRoomRepository
    {
        private IApplicationDbContext dbc;
        public ChatRoomRepository(IApplicationDbContext dbc)
        {
            this.dbc = dbc;
        }

        public IEnumerable<ChatRoom> GetAllPublicChatRooms()
        {
            return dbc.ChatRooms.Where(r => r.IsPersonalChatRoom == false).OrderByDescending(r => r.LastChanged).ToList();
        }

        public IEnumerable<ChatUsersInRoom> GetAllPersonalChatRoomsForUserIncludingRooms(ChatUser currentUser)
        {
            return dbc.ChatUsersInRooms
                .Where(ur => ur.ChatUserId == currentUser.Id && ur.ChatRoom.IsPersonalChatRoom == true)
                .OrderByDescending(ur => ur.ChatRoom.LastChanged)
                .Include(ur => ur.ChatRoom)
                .ToList();
        }


        public IEnumerable<ChatRoom> GetPersonalCommonChatRoom(int firstUserId, int secondUserId)
        {
            return dbc.ChatUsersInRooms
                .Where(ur => ur.ChatUserId == firstUserId)
                .Select(ur => ur.ChatRoom)
                    .Intersect(
                        dbc.ChatUsersInRooms
                            .Where(ur => ur.ChatUserId == secondUserId)
                            .Select(ur => ur.ChatRoom)
                    )
                    .Where(r => r.IsPersonalChatRoom == true)
                    .ToList();
        }

        public ChatUser GetPartnerForPersonalChatRoom(ChatRoom chat, ChatUser user)
        {
            return dbc.ChatUsersInRooms
                .Where(ur => ur.ChatRoomId == chat.Id && ur.ChatUserId != user.Id)
                .Select(ur => ur.ChatUser).FirstOrDefault();
        }

        public ChatRoom GetChatRoomById(int id)
        {
            return dbc.ChatRooms.Where(r => r.Id == id).FirstOrDefault();
        }

        public ChatRoom CreateNewPersonalChatRoomForUsers(ChatUser firstUser, ChatUser secondUser)
        {
            ChatRoom chatRoom = new ChatRoom() { Name = $"{firstUser.Id}+{secondUser.Id}", IsPersonalChatRoom = true, LastChanged = DateTime.Now };
            dbc.ChatRooms.Add(chatRoom);
            dbc.SaveChanges();
            dbc.ChatUsersInRooms.Add(new ChatUsersInRoom() { ChatRoom = chatRoom, ChatUser = firstUser });
            dbc.ChatUsersInRooms.Add(new ChatUsersInRoom() { ChatRoom = chatRoom, ChatUser = secondUser });
            dbc.SaveChanges();
            return chatRoom;
        }

        public void AddUserToPublicRooms(ChatUser chatUser)
        {
            var publicRooms = GetAllPublicChatRooms();
            foreach(var room in publicRooms)
            {
                dbc.ChatUsersInRooms.Add(new ChatUsersInRoom() { ChatRoom = room, ChatUser = chatUser});
            }
            dbc.SaveChanges();
        }

        public async Task AddUserToPublicRoomsAsync(ChatUser chatUser)
        {
            await Task.Run(() => AddUserToPublicRooms(chatUser));
        }

        public bool CanViewChatRoomData(ChatRoom chat, ChatUser user)
        {
            return dbc.ChatUsersInRooms
                .Where(ur => ur.ChatRoomId == chat.Id && ur.ChatUserId == user.Id)
                .Count() > 0;
        }

        public void UpdateChatRoomLastChangedDate(ChatRoom chatRoom, DateTime date)
        {
            var chat = dbc.ChatRooms.Where(u => u.Id == chatRoom.Id).FirstOrDefault();
            if (chat != null)
            {
                chat.LastChanged = date;
                dbc.SaveChanges();
            }
        }

        public async Task UpdateChatRoomLastChangedDateAsync(ChatRoom chatRoom, DateTime date)
        {
            await Task.Run(() => UpdateChatRoomLastChangedDate(chatRoom, date));
        }
    }
}