﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAppSPA.Models
{
    /// <summary>
    /// Repersents ChatRooms for client side displaying.
    /// Normaly for public chats ChatUserId ignored.
    /// For private chat it uses for profile picture downloading.
    /// If ChatRoom doesn't exists Id set to 0 (zero)
    /// and client used ChatUserId to identify new chat creation
    /// </summary>
    public class ChatRoomViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsPersonalChatRoom { get; set; }

        public int ChatUserId { get; set; }
    }
}