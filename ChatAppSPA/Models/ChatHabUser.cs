﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAppSPA.Models
{
    public class ChatHabUser
    {
        public string ConnectionId { get; set; }
        public int UserId { get; set; }
    }
}