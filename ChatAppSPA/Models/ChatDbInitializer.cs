﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

using ChatAppSPA.Models.Repositories;
using ChatAppSPA.Models.Entities;

namespace ChatAppSPA.Models
{
    public class ChatDbInitializer :
        DropCreateDatabaseIfModelChanges<ApplicationDbContext>
        //DropCreateDatabaseAlways<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext db)
        {
            db.Database.ExecuteSqlCommand("CREATE INDEX[IX_Messages_Date] ON[dbo].[Messages] ([Id]) INCLUDE ([Date])");
            db.Database.ExecuteSqlCommand("CREATE INDEX[IX_ChatRooms_LastChanged] ON[dbo].[ChatRooms]([LastChanged] DESC)");
            db.Database.ExecuteSqlCommand("CREATE INDEX[IX_ChatUsersInRooms_ALL] ON[dbo].[ChatUsersInRooms]([ChatUserId] ASC, [ChatRoomId] ASC)");
            
            

            db.Users.Add(new ApplicationUser() { Email = "1@ya.ru", UserName = "1@ya.ru" });
            db.Users.Add(new ApplicationUser() { Email = "2@ya.ru", UserName = "2@ya.ru" });
            db.Users.Add(new ApplicationUser() { Email = "3@ya.ru", UserName = "3@ya.ru" });
            db.Users.Add(new ApplicationUser() { Email = "4@ya.ru", UserName = "4@ya.ru" });
            db.Users.Add(new ApplicationUser() { Email = "5@ya.ru", UserName = "5@ya.ru" });

            db.SaveChanges();

            var appUser = db.Users.Where(u => u.Email == "1@ya.ru").First();
            db.ChatUsers.Add(new ChatUser() { Nick = "first", ApplicationUser = appUser });
            appUser = db.Users.Where(u => u.Email == "2@ya.ru").First();
            db.ChatUsers.Add(new ChatUser() { Nick = "second", ApplicationUser = appUser });
            appUser = db.Users.Where(u => u.Email == "3@ya.ru").First();
            db.ChatUsers.Add(new ChatUser() { Nick = "third", ApplicationUser = appUser });
            appUser = db.Users.Where(u => u.Email == "4@ya.ru").First();
            db.ChatUsers.Add(new ChatUser() { Nick = "fourth", ApplicationUser = appUser });
            appUser = db.Users.Where(u => u.Email == "5@ya.ru").First();
            db.ChatUsers.Add(new ChatUser() { Nick = "fifth", ApplicationUser = appUser });

            db.SaveChanges();

            // Add default public chatroom for all users
            db.ChatRooms.Add(new ChatRoom() { Name = "Public", IsPersonalChatRoom = false, LastChanged = DateTime.Now });

            db.SaveChanges();

            base.Seed(db);
        }
    }
}