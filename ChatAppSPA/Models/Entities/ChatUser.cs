﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

using Newtonsoft.Json;

namespace ChatAppSPA.Models.Entities
{
    public class ChatUser
    {
        public int Id { get; set; }

        public string Nick { get; set; }

        public bool IsDeleted { get; set; }

        [JsonIgnore]
        public virtual ApplicationUser ApplicationUser { get; set; }

        [JsonIgnore]
        public virtual ICollection<Message> PostedMessages { get; set; }

        [JsonIgnore]
        public virtual ICollection<ChatUsersInRoom> ParticipateInRooms { get; set; }
    }
}