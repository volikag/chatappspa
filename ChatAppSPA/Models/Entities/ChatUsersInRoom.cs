﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAppSPA.Models.Entities
{
    public class ChatUsersInRoom
    {
        public int Id { get; set; }

        public int ChatUserId { get; set; }
        public virtual ChatUser ChatUser { get; set; }

        public int ChatRoomId { get; set; }
        public virtual ChatRoom ChatRoom { get; set; }
    }
}