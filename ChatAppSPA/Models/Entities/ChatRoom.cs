﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

using Newtonsoft.Json;

namespace ChatAppSPA.Models.Entities
{
    public class ChatRoom
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsPersonalChatRoom { get; set; }

        public DateTime LastChanged { get; set; }

        [JsonIgnore]
        public virtual ICollection<Message> Messages { get; set; }

        [JsonIgnore]
        public virtual ICollection<ChatUsersInRoom> UsersInRoom { get; set; }
    }
}