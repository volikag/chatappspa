﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChatAppSPA.Models
{
    public class MessageViewModel
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int AddresseeId { get; set; }
        public int ChatRoomId { get; set; }
        public bool InPersonalChatRoom { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
    }
}