﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

using NUnit.Framework;
using NSubstitute;
using EntityFramework.Testing;

using ChatAppSPA;
using ChatAppSPA.Models;
using ChatAppSPA.Models.Repositories;
using ChatAppSPA.Models.Entities;


namespace ChatAppSPA.Tests.Models
{
    class ChatUserRespositoryTest
    {
        IApplicationDbContext defaultDBC;
        [SetUp]
        public void SetUp()
        {
            var dataAppUsers = new List<ApplicationUser>
                {
                    new ApplicationUser { ProfilePicture = null, ChatUser = null},
                };

            var mockAppUsers = Substitute.For<DbSet<ApplicationUser>, IQueryable<ApplicationUser>, IDbAsyncEnumerable<ApplicationUser>>()
                                                            .SetupData(dataAppUsers);

            var dataChatUsers = new List<ChatUser>
                {
                    new ChatUser { Id = 1, IsDeleted = false, Nick = "First"},
                    new ChatUser { Id = 2, IsDeleted = false, Nick = "Second"},
                    new ChatUser { Id = 3, IsDeleted = false, Nick = "Third"},
                    new ChatUser { Id = 4, IsDeleted = true, Nick = "IamDeleted"},
                };

            var mockChatUsers = Substitute.For<DbSet<ChatUser>, IQueryable<ChatUser>, IDbAsyncEnumerable<ChatUser>>()
                                                            .SetupData(dataChatUsers);

            DateTime data = new DateTime(2017, 04, 22, 00, 00, 00, 0000);

            var dataChatRooms = new List<ChatRoom>
                {
                    new ChatRoom { Id = 1, IsPersonalChatRoom = false, LastChanged = data, Name = "Public"},
                    new ChatRoom { Id = 2, IsPersonalChatRoom = true,  LastChanged = data.AddMinutes(1), Name = "1+2" },
                    new ChatRoom { Id = 3, IsPersonalChatRoom = true,  LastChanged = data.AddMinutes(2), Name = "1+3" },
                };

            var mockChatRooms = Substitute.For<DbSet<ChatRoom>, IQueryable<ChatRoom>, IDbAsyncEnumerable<ChatRoom>>()
                                                            .SetupData(dataChatRooms);


            var dataChatUsersInRooms = new List<ChatUsersInRoom>
                {
                    new ChatUsersInRoom { Id = 1, ChatRoomId = 1, ChatUserId = 1, ChatRoom = dataChatRooms[0], ChatUser= dataChatUsers[0] },
                    new ChatUsersInRoom { Id = 2, ChatRoomId = 1, ChatUserId = 2, ChatRoom = dataChatRooms[0], ChatUser= dataChatUsers[1] },
                    new ChatUsersInRoom { Id = 3, ChatRoomId = 1, ChatUserId = 3, ChatRoom = dataChatRooms[0], ChatUser= dataChatUsers[2] },
                    new ChatUsersInRoom { Id = 4, ChatRoomId = 2, ChatUserId = 1, ChatRoom = dataChatRooms[1], ChatUser= dataChatUsers[0]},
                    new ChatUsersInRoom { Id = 5, ChatRoomId = 2, ChatUserId = 2, ChatRoom = dataChatRooms[1], ChatUser= dataChatUsers[1] },
                    new ChatUsersInRoom { Id = 6, ChatRoomId = 3, ChatUserId = 1, ChatRoom = dataChatRooms[2], ChatUser= dataChatUsers[0] },
                    new ChatUsersInRoom { Id = 7, ChatRoomId = 3, ChatUserId = 3, ChatRoom = dataChatRooms[2], ChatUser= dataChatUsers[2] },
                };

            var mockChatUsersInRooms = Substitute.For<DbSet<ChatUsersInRoom>, IQueryable<ChatUsersInRoom>, IDbAsyncEnumerable<ChatUsersInRoom>>()
                                                            .SetupData(dataChatUsersInRooms);

            var dataMessages = new List<Message>
                {
                    new Message { Id = 1, ChatRoomId = 1, AuthorId = 1, ChatRoom = dataChatRooms[0], Author = dataChatUsers[0], Date = data.AddMinutes(1), Text = "1" },
                    new Message { Id = 2, ChatRoomId = 1, AuthorId = 2, ChatRoom = dataChatRooms[0], Author = dataChatUsers[1], Date = data.AddMinutes(2), Text = "2" },
                    new Message { Id = 3, ChatRoomId = 1, AuthorId = 3, ChatRoom = dataChatRooms[0], Author = dataChatUsers[2], Date = data.AddMinutes(3), Text = "3" },
                    new Message { Id = 4, ChatRoomId = 1, AuthorId = 1, ChatRoom = dataChatRooms[0], Author = dataChatUsers[0], Date = data.AddMinutes(4), Text = "4" },
                    new Message { Id = 5, ChatRoomId = 2, AuthorId = 2, ChatRoom = dataChatRooms[1], Author = dataChatUsers[1], Date = data.AddMinutes(5), Text = "5" },
                    new Message { Id = 6, ChatRoomId = 2, AuthorId = 1, ChatRoom = dataChatRooms[1], Author = dataChatUsers[0], Date = data.AddMinutes(6), Text = "6" },
                    new Message { Id = 7, ChatRoomId = 3, AuthorId = 3, ChatRoom = dataChatRooms[2], Author = dataChatUsers[2], Date = data.AddMinutes(7), Text = "7" },
                };

            var mockMessages = Substitute.For<DbSet<Message>, IQueryable<Message>, IDbAsyncEnumerable<Message>>()
                                                            .SetupData(dataMessages);

            defaultDBC = Substitute.For<IApplicationDbContext>();
            defaultDBC.ChatUsers.Returns(mockChatUsers);
            defaultDBC.ChatRooms.Returns(mockChatRooms);
            defaultDBC.ChatUsersInRooms.Returns(mockChatUsersInRooms);
            defaultDBC.Messages.Returns(mockMessages);
        }

        #region GetAllUsers


        [Test]
        public void GetAllUsers_ForEmptyUsersList_ReturnsEmptyCollection()
        {
            var dataChatUsers = new List<ChatUser>();

            var mockChatUsers = Substitute.For<DbSet<ChatUser>, IQueryable<ChatUser>, IDbAsyncEnumerable<ChatUser>>()
                                                            .SetupData(dataChatUsers);

            var dbc = Substitute.For<IApplicationDbContext>();
            dbc.ChatUsers.Returns(mockChatUsers);

            IChatUserRepository repo = new ChatUserRespository(dbc);

            // Act
            var actual = repo.GetAllUsers();

            //Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(0, actual.Count());
        }

        [Test]
        public void GetAllUsers_ForThreeNormalAndOneDeleted_ReturnsThreeUsers()
        {
            IChatUserRepository repo = new ChatUserRespository(defaultDBC);

            ChatRoom chat = new ChatRoom() { Id = 1 };
            ChatUser author = new ChatUser() { Id = 2 };

            // Act
            var aqtual = repo.GetAllUsers();

            //Assert
            Assert.IsNotNull(aqtual);
            Assert.AreEqual(3, aqtual.Count());

            var user = aqtual.First();

            Assert.AreEqual(1, user.Id);
            Assert.AreEqual("First", user.Nick);

            user = aqtual.Last();

            Assert.AreEqual(3, user.Id);
            Assert.AreEqual("Third", user.Nick);
        }

        #endregion

        #region GetById

        [Test]
        public void GetById_GetsNonExistedUser_ReturnsNull()
        {
            IChatUserRepository repo = new ChatUserRespository(defaultDBC);

            // Act
            var user = repo.GetById(50);

            //Assert
            Assert.IsNull(user);
        }

        [Test]
        public void GetById_GetsDeletedUser_ReturnsNull()
        {
            IChatUserRepository repo = new ChatUserRespository(defaultDBC);

            // Act
            var user = repo.GetById(4);

            //Assert
            Assert.IsNull(user);
        }

        [Test]
        public void GetById_GetsNormalUser_ReturnsNull()
        {
            IChatUserRepository repo = new ChatUserRespository(defaultDBC);

            // Act
            var user = repo.GetById(1);

            //Assert
            Assert.IsNotNull(user);

            Assert.AreEqual(1, user.Id);
            Assert.AreEqual("First", user.Nick);
        }

        #endregion

    }
}
