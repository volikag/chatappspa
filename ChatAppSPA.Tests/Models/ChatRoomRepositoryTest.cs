﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

using NUnit.Framework;
using NSubstitute;
using EntityFramework.Testing;


using ChatAppSPA;
using ChatAppSPA.Models;
using ChatAppSPA.Models.Repositories;
using ChatAppSPA.Models.Entities;


namespace ChatAppSPA.Tests.Models
{
    [TestFixture]
    class ChatRoomRepositoryTest
    {
        IApplicationDbContext defaultDBC;
        [SetUp]
        public void SetUp()
        {
            var dataAppUsers = new List<ApplicationUser>
                {
                    new ApplicationUser { ProfilePicture = null, ChatUser = null},
                };

            var mockAppUsers = Substitute.For<DbSet<ApplicationUser>, IQueryable<ApplicationUser>, IDbAsyncEnumerable<ApplicationUser>>()
                                                            .SetupData(dataAppUsers);

            var dataChatUsers = new List<ChatUser>
                {
                    new ChatUser { Id = 1, IsDeleted = false, Nick = "First"},
                    new ChatUser { Id = 2, IsDeleted = false, Nick = "Second"},
                    new ChatUser { Id = 3, IsDeleted = false, Nick = "Third"},
                    new ChatUser { Id = 4, IsDeleted = true, Nick = "IamDeleted"},
                };

            var mockChatUsers = Substitute.For<DbSet<ChatUser>, IQueryable<ChatUser>, IDbAsyncEnumerable<ChatUser>>()
                                                            .SetupData(dataChatUsers);

            DateTime data = new DateTime(2017, 04, 22, 00, 00, 00, 0000);

            var dataChatRooms = new List<ChatRoom>
                {
                    new ChatRoom { Id = 1, IsPersonalChatRoom = false, LastChanged = data, Name = "Public"},
                    new ChatRoom { Id = 2, IsPersonalChatRoom = true,  LastChanged = data.AddMinutes(1), Name = "1+2" },
                    new ChatRoom { Id = 3, IsPersonalChatRoom = true,  LastChanged = data.AddMinutes(2), Name = "1+3" },
                };

            var mockChatRooms = Substitute.For<DbSet<ChatRoom>, IQueryable<ChatRoom>, IDbAsyncEnumerable<ChatRoom>>()
                                                            .SetupData(dataChatRooms);


            var dataChatUsersInRooms = new List<ChatUsersInRoom>
                {
                    new ChatUsersInRoom { Id = 1, ChatRoomId = 1, ChatUserId = 1, ChatRoom = dataChatRooms[0], ChatUser= dataChatUsers[0] },
                    new ChatUsersInRoom { Id = 2, ChatRoomId = 1, ChatUserId = 2, ChatRoom = dataChatRooms[0], ChatUser= dataChatUsers[1] },
                    new ChatUsersInRoom { Id = 3, ChatRoomId = 1, ChatUserId = 3, ChatRoom = dataChatRooms[0], ChatUser= dataChatUsers[2] },
                    new ChatUsersInRoom { Id = 4, ChatRoomId = 2, ChatUserId = 1, ChatRoom = dataChatRooms[1], ChatUser= dataChatUsers[0]},
                    new ChatUsersInRoom { Id = 5, ChatRoomId = 2, ChatUserId = 2, ChatRoom = dataChatRooms[1], ChatUser= dataChatUsers[1] },
                    new ChatUsersInRoom { Id = 6, ChatRoomId = 3, ChatUserId = 1, ChatRoom = dataChatRooms[2], ChatUser= dataChatUsers[0] },
                    new ChatUsersInRoom { Id = 7, ChatRoomId = 3, ChatUserId = 3, ChatRoom = dataChatRooms[2], ChatUser= dataChatUsers[2] },
                };

            var mockChatUsersInRooms = Substitute.For<DbSet<ChatUsersInRoom>, IQueryable<ChatUsersInRoom>, IDbAsyncEnumerable<ChatUsersInRoom>>()
                                                            .SetupData(dataChatUsersInRooms);

            defaultDBC = Substitute.For<IApplicationDbContext>();
            defaultDBC.ChatUsers.Returns(mockChatUsers);
            defaultDBC.ChatRooms.Returns(mockChatRooms);
            defaultDBC.ChatUsersInRooms.Returns(mockChatUsersInRooms);
        }

        #region GetAllPublicChatRooms
        [Test]
        public void GetAllPublicChatRooms_WithNoChats_ReturnsZeroChats()
        {
            var dataChatRooms = new List<ChatRoom>();

            var mockChatRooms = Substitute.For<DbSet<ChatRoom>, IQueryable<ChatRoom>, IDbAsyncEnumerable<ChatRoom>>()
                                            .SetupData(dataChatRooms);

            var dbc = Substitute.For<IApplicationDbContext>();
            dbc.ChatRooms.Returns(mockChatRooms);
            IChatRoomRepository repo = new ChatRoomRepository(dbc);

            var result = repo.GetAllPublicChatRooms();

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void GetAllPublicChatRooms_WithNoPublicChats_ReturnsZeroChats()
        {
            var dataChatRooms = new List<ChatRoom>
                {
                    new ChatRoom { Id = 2, IsPersonalChatRoom = true,  LastChanged = DateTime.Now, Name = "1+2" },
                    new ChatRoom { Id = 3, IsPersonalChatRoom = true,  LastChanged = DateTime.Now, Name = "1+3" },
                };

            var mockChatRooms = Substitute.For<DbSet<ChatRoom>, IQueryable<ChatRoom>, IDbAsyncEnumerable<ChatRoom>>()
                                            .SetupData(dataChatRooms);

            var dbc = Substitute.For<IApplicationDbContext>();
            dbc.ChatRooms.Returns(mockChatRooms);
            IChatRoomRepository repo = new ChatRoomRepository(dbc);

            var result = repo.GetAllPublicChatRooms();

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void GetAllPublicChatRooms_WithOnePublicChat_ReturnsOneChat()
        {
            IChatRoomRepository repo = new ChatRoomRepository(defaultDBC);

            var result = repo.GetAllPublicChatRooms();

            var chat = result.First();

            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(1, chat.Id);
            Assert.AreEqual(false, chat.IsPersonalChatRoom);
        }

        [Test]
        public void GetAllPublicChatRooms_WithTwoPublicChats_ReturnsTwoChat()
        {
            var dataChatRooms = new List<ChatRoom>
                {
                    new ChatRoom { Id = 1, IsPersonalChatRoom = false,  LastChanged = DateTime.Now, Name = "Public1" },
                    new ChatRoom { Id = 2, IsPersonalChatRoom = true,  LastChanged = DateTime.Now, Name = "1+2" },
                    new ChatRoom { Id = 3, IsPersonalChatRoom = true,  LastChanged = DateTime.Now, Name = "1+3" },
                    new ChatRoom { Id = 4, IsPersonalChatRoom = false,  LastChanged = DateTime.Now, Name = "Public2" },
                };

            var mockChatRooms = Substitute.For<DbSet<ChatRoom>, IQueryable<ChatRoom>, IDbAsyncEnumerable<ChatRoom>>()
                                            .SetupData(dataChatRooms);

            var dbc = Substitute.For<IApplicationDbContext>();
            dbc.ChatRooms.Returns(mockChatRooms);
            IChatRoomRepository repo = new ChatRoomRepository(dbc);

            var result = repo.GetAllPublicChatRooms();

            var chat = result.First();

            Assert.AreEqual(2, result.Count());
            Assert.AreEqual(1, chat.Id);
            Assert.AreEqual(false, chat.IsPersonalChatRoom);

            chat = result.Last();
            Assert.AreEqual(4, chat.Id);
            Assert.AreEqual(false, chat.IsPersonalChatRoom);
        }

        #endregion


        #region GetAllPersonalChatRoomsForUserIncludingRooms

        [Test]
        public void GetAllPersonalChatRoomsForUserIncludingRooms_WithNoChats_ReturnsZeroChats()
        {
            var dataChatUsersInRooms = new List<ChatUsersInRoom>();

            var mockChatUsersInRooms = Substitute.For<DbSet<ChatUsersInRoom>, IQueryable<ChatUsersInRoom>, IDbAsyncEnumerable<ChatUsersInRoom>>()
                                                            .SetupData(dataChatUsersInRooms);

            var dbc = Substitute.For<IApplicationDbContext>();
            dbc.ChatUsersInRooms.Returns(mockChatUsersInRooms);

            IChatRoomRepository repo = new ChatRoomRepository(dbc);

            ChatUser user = new ChatUser() { Id = 1 };

            var result = repo.GetAllPersonalChatRoomsForUserIncludingRooms(user);

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void GetAllPersonalChatRoomsForUserIncludingRooms_WithTwoPubliChats_ReturnsZeroChats()
        {
            var dataChatRooms = new List<ChatRoom>
                {
                    new ChatRoom { Id = 1, IsPersonalChatRoom = false,  LastChanged = DateTime.Now, Name = "Public1" },
                    new ChatRoom { Id = 2, IsPersonalChatRoom = false,  LastChanged = DateTime.Now, Name = "Public2" },
                };

            var mockChatRooms = Substitute.For<DbSet<ChatRoom>, IQueryable<ChatRoom>, IDbAsyncEnumerable<ChatRoom>>()
                                            .SetupData(dataChatRooms);

            var dataChatUsersInRooms = new List<ChatUsersInRoom>
                {
                    new ChatUsersInRoom { Id = 1, ChatRoomId = 1, ChatUserId = 1, ChatRoom = dataChatRooms[0] },
                    new ChatUsersInRoom { Id = 2, ChatRoomId = 1, ChatUserId = 2, ChatRoom = dataChatRooms[0] },
                    new ChatUsersInRoom { Id = 3, ChatRoomId = 1, ChatUserId = 3, ChatRoom = dataChatRooms[0] },
                    new ChatUsersInRoom { Id = 4, ChatRoomId = 2, ChatUserId = 1, ChatRoom = dataChatRooms[1] },
                    new ChatUsersInRoom { Id = 5, ChatRoomId = 2, ChatUserId = 2, ChatRoom = dataChatRooms[1] },
                    new ChatUsersInRoom { Id = 6, ChatRoomId = 2, ChatUserId = 3, ChatRoom = dataChatRooms[1] },
                };

            var mockChatUsersInRooms = Substitute.For<DbSet<ChatUsersInRoom>, IQueryable<ChatUsersInRoom>, IDbAsyncEnumerable<ChatUsersInRoom>>()
                                                            .SetupData(dataChatUsersInRooms);

            var dbc = Substitute.For<IApplicationDbContext>();
            dbc.ChatRooms.Returns(mockChatRooms);
            dbc.ChatUsersInRooms.Returns(mockChatUsersInRooms);

            IChatRoomRepository repo = new ChatRoomRepository(dbc);

            ChatUser user = new ChatUser() { Id = 1 };

            var result = repo.GetAllPersonalChatRoomsForUserIncludingRooms(user);


            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void GetAllPersonalChatRoomsForUserIncludingRooms_WithOtherUserPersonalChat_ReturnsZeroChats()
        {
            IChatRoomRepository repo = new ChatRoomRepository(defaultDBC);


            ChatUser user = new ChatUser() { Id = 4 };

            var result = repo.GetAllPersonalChatRoomsForUserIncludingRooms(user);

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void GetAllPersonalChatRoomsForUserIncludingRooms_WithTwoPersonalChats_ReturnsTwoChats()
        {
            IChatRoomRepository repo = new ChatRoomRepository(defaultDBC);

            ChatUser user = new ChatUser() { Id = 1 };

            var result = repo.GetAllPersonalChatRoomsForUserIncludingRooms(user);

            var ur = result.First();
            Assert.AreEqual(2, result.Count());
            Assert.AreEqual(1, ur.ChatUserId);
            Assert.AreEqual(3, ur.ChatRoomId);
            Assert.AreEqual(3, ur.ChatRoom.Id);
            Assert.AreEqual(true, ur.ChatRoom.IsPersonalChatRoom);

            ur = result.Last();
            Assert.AreEqual(1, ur.ChatUserId);
            Assert.AreEqual(2, ur.ChatRoomId);
            Assert.AreEqual(2, ur.ChatRoom.Id);
            Assert.AreEqual(true, ur.ChatRoom.IsPersonalChatRoom);
        }
        #endregion


        #region GetPersonalCommonChatRoom

        [Test]
        public void GetPersonalCommonChatRoom_WithNoChats_ReturnsZeroChats()
        {
            var dataChatUsersInRooms = new List<ChatUsersInRoom>();

            var mockChatUsersInRooms = Substitute.For<DbSet<ChatUsersInRoom>, IQueryable<ChatUsersInRoom>, IDbAsyncEnumerable<ChatUsersInRoom>>()
                                                            .SetupData(dataChatUsersInRooms);

            var dbc = Substitute.For<IApplicationDbContext>();
            dbc.ChatUsersInRooms.Returns(mockChatUsersInRooms);

            IChatRoomRepository repo = new ChatRoomRepository(dbc);

            ChatUser user1 = new ChatUser() { Id = 1 };
            ChatUser user2 = new ChatUser() { Id = 2 };

            var result = repo.GetPersonalCommonChatRoom(user1.Id, user2.Id);

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void GetPersonalCommonChatRoom_WithTwoPubliChats_ReturnsZeroChats()
        {
            var dataChatRooms = new List<ChatRoom>
                {
                    new ChatRoom { Id = 1, IsPersonalChatRoom = false,  LastChanged = DateTime.Now, Name = "Public1" },
                    new ChatRoom { Id = 2, IsPersonalChatRoom = false,  LastChanged = DateTime.Now, Name = "Public2" },
                };

            var mockChatRooms = Substitute.For<DbSet<ChatRoom>, IQueryable<ChatRoom>, IDbAsyncEnumerable<ChatRoom>>()
                                            .SetupData(dataChatRooms);

            var dataChatUsersInRooms = new List<ChatUsersInRoom>
                {
                    new ChatUsersInRoom { Id = 1, ChatRoomId = 1, ChatUserId = 1, ChatRoom = dataChatRooms[0] },
                    new ChatUsersInRoom { Id = 2, ChatRoomId = 1, ChatUserId = 2, ChatRoom = dataChatRooms[0] },
                    new ChatUsersInRoom { Id = 3, ChatRoomId = 1, ChatUserId = 3, ChatRoom = dataChatRooms[0] },
                    new ChatUsersInRoom { Id = 4, ChatRoomId = 2, ChatUserId = 1, ChatRoom = dataChatRooms[1] },
                    new ChatUsersInRoom { Id = 5, ChatRoomId = 2, ChatUserId = 2, ChatRoom = dataChatRooms[1] },
                    new ChatUsersInRoom { Id = 6, ChatRoomId = 2, ChatUserId = 3, ChatRoom = dataChatRooms[1] },
                };

            var mockChatUsersInRooms = Substitute.For<DbSet<ChatUsersInRoom>, IQueryable<ChatUsersInRoom>, IDbAsyncEnumerable<ChatUsersInRoom>>()
                                                            .SetupData(dataChatUsersInRooms);

            var dbc = Substitute.For<IApplicationDbContext>();
            dbc.ChatRooms.Returns(mockChatRooms);
            dbc.ChatUsersInRooms.Returns(mockChatUsersInRooms);

            IChatRoomRepository repo = new ChatRoomRepository(dbc);

            ChatUser user1 = new ChatUser() { Id = 1 };
            ChatUser user2 = new ChatUser() { Id = 2 };

            var result = repo.GetPersonalCommonChatRoom(user1.Id, user2.Id);


            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void GetPersonalCommonChatRoom_WithNoCommonPersonalChat_ReturnsZeroChats()
        {
            IChatRoomRepository repo = new ChatRoomRepository(defaultDBC);


            ChatUser user1 = new ChatUser() { Id = 2 };
            ChatUser user2 = new ChatUser() { Id = 3 };

            var result = repo.GetPersonalCommonChatRoom(user1.Id, user2.Id);

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void GetPersonalCommonChatRoom_WithPersonalChat_ReturnsOneChat()
        {
            IChatRoomRepository repo = new ChatRoomRepository(defaultDBC);

            ChatUser user1 = new ChatUser() { Id = 1 };
            ChatUser user2 = new ChatUser() { Id = 2 };

            var result = repo.GetPersonalCommonChatRoom(user1.Id, user2.Id);

            var chat = result.First();
            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(2, chat.Id);
            Assert.AreEqual(true, chat.IsPersonalChatRoom);
            Assert.AreEqual("1+2", chat.Name);
        }

        #endregion


        #region GetPartnerForPersonalChatRoom

        [Test]
        public void GetPartnerForPersonalChatRoom_WithNoChats_ReturnsZeroChats()
        {
            var dataChatUsersInRooms = new List<ChatUsersInRoom>();

            var mockChatUsersInRooms = Substitute.For<DbSet<ChatUsersInRoom>, IQueryable<ChatUsersInRoom>, IDbAsyncEnumerable<ChatUsersInRoom>>()
                                                            .SetupData(dataChatUsersInRooms);

            var dbc = Substitute.For<IApplicationDbContext>();
            dbc.ChatUsersInRooms.Returns(mockChatUsersInRooms);

            IChatRoomRepository repo = new ChatRoomRepository(dbc);

            ChatRoom room = new ChatRoom() { Id = 1 };
            ChatUser user = new ChatUser() { Id = 1 };

            var result = repo.GetPartnerForPersonalChatRoom(room, user);

            Assert.AreEqual(null, result);
        }

        [Test]
        public void GetPartnerForPersonalChatRoom_WithTwoPublicChats_ReturnsZeroChats()
        {
            var dataChatRooms = new List<ChatRoom>
                {
                    new ChatRoom { Id = 1, IsPersonalChatRoom = false,  LastChanged = DateTime.Now, Name = "Public1" },
                    new ChatRoom { Id = 2, IsPersonalChatRoom = false,  LastChanged = DateTime.Now, Name = "Public2" },
                };

            var mockChatRooms = Substitute.For<DbSet<ChatRoom>, IQueryable<ChatRoom>, IDbAsyncEnumerable<ChatRoom>>()
                                            .SetupData(dataChatRooms);

            var dataChatUsersInRooms = new List<ChatUsersInRoom>
                {
                    new ChatUsersInRoom { Id = 1, ChatRoomId = 1, ChatUserId = 1, ChatRoom = dataChatRooms[0] },
                    new ChatUsersInRoom { Id = 2, ChatRoomId = 1, ChatUserId = 2, ChatRoom = dataChatRooms[0] },
                    new ChatUsersInRoom { Id = 3, ChatRoomId = 1, ChatUserId = 3, ChatRoom = dataChatRooms[0] },
                    new ChatUsersInRoom { Id = 4, ChatRoomId = 2, ChatUserId = 1, ChatRoom = dataChatRooms[1] },
                    new ChatUsersInRoom { Id = 5, ChatRoomId = 2, ChatUserId = 2, ChatRoom = dataChatRooms[1] },
                    new ChatUsersInRoom { Id = 6, ChatRoomId = 2, ChatUserId = 3, ChatRoom = dataChatRooms[1] },
                };

            var mockChatUsersInRooms = Substitute.For<DbSet<ChatUsersInRoom>, IQueryable<ChatUsersInRoom>, IDbAsyncEnumerable<ChatUsersInRoom>>()
                                                            .SetupData(dataChatUsersInRooms);

            var dbc = Substitute.For<IApplicationDbContext>();
            dbc.ChatRooms.Returns(mockChatRooms);
            dbc.ChatUsersInRooms.Returns(mockChatUsersInRooms);

            IChatRoomRepository repo = new ChatRoomRepository(dbc);

            ChatRoom room = new ChatRoom() { Id = 1 };
            ChatUser user = new ChatUser() { Id = 1 };

            var result = repo.GetPartnerForPersonalChatRoom(room, user);


            Assert.AreEqual(null, result);
        }

        //[Test]
        //public void GetPartnerForPersonalChatRoom_WithOthersPersonalChat_ReturnsZeroChats()
        //{
        //    IChatRoomRepository repo = new ChatRoomRepository(defaultDBC);

        //    ChatRoom room = new ChatRoom() { Id = 2 };
        //    ChatUser user = new ChatUser() { Id = 3 };

        //    var result = repo.GetPartnerForPersonalChatRoom(room, user);

        //    Assert.AreEqual(null, result);
        //}

        [Test]
        public void GetPartnerForPersonalChatRoom_WithCommonPersonalChatForFirstUser_ReturnsOneChat()
        {
            IChatRoomRepository repo = new ChatRoomRepository(defaultDBC);

            ChatRoom room = new ChatRoom() { Id = 2 };
            ChatUser firstUser = new ChatUser() { Id = 1 };

            var user = repo.GetPartnerForPersonalChatRoom(room, firstUser);

            Assert.AreEqual(2, user.Id);
        }

        [Test]
        public void GetPartnerForPersonalChatRoom_WithCommonPersonalChatForSecondUser_ReturnsOneChat()
        {
            IChatRoomRepository repo = new ChatRoomRepository(defaultDBC);

            ChatRoom room = new ChatRoom() { Id = 2 };
            ChatUser firstUser = new ChatUser() { Id = 2 };

            var user = repo.GetPartnerForPersonalChatRoom(room, firstUser);

            Assert.AreEqual(1, user.Id);
        }

        #endregion


        #region CreateNewPersonalChatRoomForUsers

        [Test]
        public void CreateNewPersonalChatRoomForUsers_ForTwoUsers_ReturnsNewChats()
        {
            var dataChatUsersInRooms = new List<ChatUsersInRoom>();

            var mockChatUsersInRooms = Substitute.For<DbSet<ChatUsersInRoom>, IQueryable<ChatUsersInRoom>, IDbAsyncEnumerable<ChatUsersInRoom>>()
                                                            .SetupData(dataChatUsersInRooms);

            var dbc = Substitute.For<IApplicationDbContext>();
            dbc.ChatUsersInRooms.Returns(mockChatUsersInRooms);

            IChatRoomRepository repo = new ChatRoomRepository(dbc);

            ChatUser user1 = new ChatUser() { Id = 1 };
            ChatUser user2 = new ChatUser() { Id = 2 };

            // Act
            var result = repo.CreateNewPersonalChatRoomForUsers(user1, user2);

            //Assert
            Assert.IsNotNull(result);

            dbc.ChatRooms.Received().Add(Arg.Is<ChatRoom>(r => r.Name == "1+2"));
            dbc.Received().SaveChanges();
            dbc.ChatUsersInRooms.Received().Add(Arg.Is<ChatUsersInRoom>(x => x.ChatUser == user1 && x.ChatRoom == result));
            dbc.ChatUsersInRooms.Received().Add(Arg.Is<ChatUsersInRoom>(x => x.ChatUser == user2 && x.ChatRoom == result));
            dbc.Received().SaveChanges();
        }

        #endregion


        #region GetChatRoomById

        [Test]
        public void GetChatRoomById_ForEmptyRooms_ReturnsNull()
        {
            var dataChatRooms = new List<ChatRoom>();

            var mockChatRooms = Substitute.For<DbSet<ChatRoom>, IQueryable<ChatRoom>, IDbAsyncEnumerable<ChatRoom>>()
                                            .SetupData(dataChatRooms);

            var dbc = Substitute.For<IApplicationDbContext>();
            dbc.ChatRooms.Returns(mockChatRooms);
            IChatRoomRepository repo = new ChatRoomRepository(dbc);

            // Act
            var result = repo.GetChatRoomById(2);

            //Assert
            Assert.IsNull(result);
        }

        [Test]
        public void GetChatRoomById_ForNonExistedRoom_ReturnsNull()
        {
            IChatRoomRepository repo = new ChatRoomRepository(defaultDBC);

            // Act
            var result = repo.GetChatRoomById(10);

            //Assert
            Assert.IsNull(result);
        }

        [Test]
        public void GetChatRoomById_ForExistedRoom_ReturnsGivenRoom()
        {
            IChatRoomRepository repo = new ChatRoomRepository(defaultDBC);

            // Act
            var result = repo.GetChatRoomById(1);

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Id);
        }

        #endregion


        #region CanViewChatRoomData

        [Test]
        public void CanViewChatRoomData_WithNoChats_ReturnsFalse()
        {
            var dataChatUsersInRooms = new List<ChatUsersInRoom>();

            var mockChatUsersInRooms = Substitute.For<DbSet<ChatUsersInRoom>, IQueryable<ChatUsersInRoom>, IDbAsyncEnumerable<ChatUsersInRoom>>()
                                                            .SetupData(dataChatUsersInRooms);

            var dbc = Substitute.For<IApplicationDbContext>();
            dbc.ChatUsersInRooms.Returns(mockChatUsersInRooms);

            IChatRoomRepository repo = new ChatRoomRepository(dbc);

            ChatRoom room = new ChatRoom() { Id = 1 };
            ChatUser user = new ChatUser() { Id = 1 };

            var actual = repo.CanViewChatRoomData(room, user);

            Assert.IsFalse(actual);
        }

        [Test]
        public void CanViewChatRoomData_WithNonGrantedAccesForPersonalChat_ReturnsFalse()
        {
            IChatRoomRepository repo = new ChatRoomRepository(defaultDBC);

            ChatRoom room = new ChatRoom() { Id = 2 };
            ChatUser firstUser = new ChatUser() { Id = 3 };

            var actual = repo.CanViewChatRoomData(room, firstUser);

            Assert.IsFalse(actual);
        }

        [Test]
        public void CanViewChatRoomData_WithGrantedAccesForPersonalChat_ReturnsTrue()
        {
            IChatRoomRepository repo = new ChatRoomRepository(defaultDBC);

            ChatRoom room = new ChatRoom() { Id = 2 };
            ChatUser firstUser = new ChatUser() { Id = 1 };

            var actual = repo.CanViewChatRoomData(room, firstUser);

            Assert.IsTrue(actual);

            ChatUser secondUser = new ChatUser() { Id = 2 };

            actual = repo.CanViewChatRoomData(room, secondUser);

            Assert.IsTrue(actual);
        }


        [Test]
        public void CanViewChatRoomData_WithGrantedAccesForPublicChat_ReturnsTrue()
        {
            IChatRoomRepository repo = new ChatRoomRepository(defaultDBC);

            ChatRoom room = new ChatRoom() { Id = 1 };
            ChatUser firstUser = new ChatUser() { Id = 2 };

            var actual = repo.CanViewChatRoomData(room, firstUser);

            Assert.IsTrue(actual);
        }

        #endregion


        #region AddUserToPublicRooms

        [Test]
        public void AddUserToPublicRooms_ForEmptyRooms_ReturnsNull()
        {
            var dataChatRooms = new List<ChatRoom>();

            var mockChatRooms = Substitute.For<DbSet<ChatRoom>, IQueryable<ChatRoom>, IDbAsyncEnumerable<ChatRoom>>()
                                            .SetupData(dataChatRooms);

            var dataChatUsersInRooms = new List<ChatUsersInRoom>();

            var mockChatUsersInRooms = Substitute.For<DbSet<ChatUsersInRoom>, IQueryable<ChatUsersInRoom>, IDbAsyncEnumerable<ChatUsersInRoom>>()
                                                            .SetupData(dataChatUsersInRooms);

            var dbc = Substitute.For<IApplicationDbContext>();
            dbc.ChatRooms.Returns(mockChatRooms);
            dbc.ChatUsersInRooms.Returns(mockChatUsersInRooms);

            IChatRoomRepository repo = new ChatRoomRepository(dbc);

            ChatUser user = new ChatUser() { Id = 1 };

            // Act
            repo.AddUserToPublicRooms(user);

            //Assert
            dbc.ChatUsersInRooms.DidNotReceive().Add(Arg.Any<ChatUsersInRoom>());
            dbc.Received().SaveChanges();
        }

        [Test]
        public void AddUserToPublicRooms_ForOnePublicRoom_SaveChanges()
        {
            IChatRoomRepository repo = new ChatRoomRepository(defaultDBC);

            ChatUser user = new ChatUser() { Id = 1 };

            // Act
            repo.AddUserToPublicRooms(user);

            //Assert
            defaultDBC.ChatUsersInRooms.Received().Add(Arg.Is<ChatUsersInRoom>(x => x.ChatUser == user && x.ChatRoom.Id == 1));
            defaultDBC.Received().SaveChanges();
        }

        #endregion


        #region UpdateChatRoomLastChangedDate

        [Test]
        public void UpdateChatRoomLastChangedDate_ForExistedRoom_ChangeDate()
        {
            IChatRoomRepository repo = new ChatRoomRepository(defaultDBC);

            var chat = defaultDBC.ChatRooms.First();
            DateTime date = DateTime.Now;
            // Act
            repo.UpdateChatRoomLastChangedDate(chat, date);

            //Assert
            defaultDBC.Received().SaveChanges();
            Assert.AreEqual(date, chat.LastChanged);
        }

        #endregion
    }
}
