﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

using NUnit.Framework;
using NSubstitute;

using ChatAppSPA.Controllers;
using ChatAppSPA.Models.Repositories;
using ChatAppSPA.Models.Entities;


namespace ChatAppSPA.Tests.Controllers
{
    [TestFixture]
    class ChatControllerTest
    {
        #region RetrieveCurrentUserData

        [Test]
        public void RetrieveCurrentUserData_GetUserDataForExistedUser_ReturnsOkWithChatUser()
        {
            var usersRepo = Substitute.For<IChatUserRepository>();
            var roomsRepo = Substitute.For<IChatRoomRepository>();

            string uid = "uid";

            var user = new ChatUser() { Id = 1, Nick = "U1", IsDeleted = false };

            usersRepo.GetByAppUserIdAsync(uid).ReturnsForAnyArgs(Task.Run(() => { return user; }));

            ChatController c = new ChatController(usersRepo, roomsRepo);

            var t = c.RetrieveCurrentUserData();
            t.Wait();

            var result = t.Result;

            Assert.IsInstanceOf(typeof(OkNegotiatedContentResult<ChatUser>), result);

            var value = result as OkNegotiatedContentResult<ChatUser>;
            Assert.AreEqual(user, value.Content);

            usersRepo.DidNotReceive().CreateNewChatUserForAppUser(uid);
            roomsRepo.DidNotReceive().AddUserToPublicRoomsAsync(user);
        }

        [Test]
        public void RetrieveCurrentUserData_GetUserDataForNonExistedUserWithSuccesCreation_ReturnsOkWithNewUser()
        {
            var usersRepo = Substitute.For<IChatUserRepository>();
            var roomsRepo = Substitute.For<IChatRoomRepository>();

            string uid = "uid";

            var user = new ChatUser() { Id = 1, Nick = "U1", IsDeleted = false };

            usersRepo.GetByAppUserIdAsync(uid).Returns(new Task<ChatUser>(() => { return null; }));
            usersRepo.CreateNewChatUserForAppUser(uid).ReturnsForAnyArgs(user);

            ChatController c = new ChatController(usersRepo, roomsRepo);

            var t = c.RetrieveCurrentUserData();
            t.Wait();

            var result = t.Result;

            Assert.IsInstanceOf(typeof(OkNegotiatedContentResult<ChatUser>), result);

            var value = result as OkNegotiatedContentResult<ChatUser>;
            Assert.AreEqual(user, value.Content);

            usersRepo.ReceivedWithAnyArgs().CreateNewChatUserForAppUser(uid);
            roomsRepo.Received().AddUserToPublicRoomsAsync(user);

        }

        [Test]
        public void RetrieveCurrentUserData_GetUserDataForNonExistedUserWithErrorCreation_ReturnsNotFoundResult()
        {
            var usersRepo = Substitute.For<IChatUserRepository>();
            var roomsRepo = Substitute.For<IChatRoomRepository>();

            string uid = "uid";

            var user = new ChatUser() { Id = 1, Nick = "U1", IsDeleted = false };

            usersRepo.GetByAppUserIdAsync(uid).Returns(new Task<ChatUser>(() => { return null; }));
            usersRepo.CreateNewChatUserForAppUser(uid).Returns<ChatUser>(user);

            ChatController c = new ChatController(usersRepo, roomsRepo);

            var t = c.RetrieveCurrentUserData();
            t.Wait();

            var r = t.Result;

            Assert.IsInstanceOf(typeof(NotFoundResult), r);

            usersRepo.ReceivedWithAnyArgs().CreateNewChatUserForAppUser(uid);
            roomsRepo.ReceivedWithAnyArgs().AddUserToPublicRoomsAsync(user);

        }

        #endregion

        #region GetUsersList

        [Test]
        public void GetUsersList_GetFromEmptyUserList_ReturnsEmptyCollection()
        {
            var usersRepo = Substitute.For<IChatUserRepository>();
            var roomsRepo = Substitute.For<IChatRoomRepository>();


            usersRepo.GetAllUsers().Returns(new List<ChatUser>());

            ChatController c = new ChatController(usersRepo, roomsRepo);

            var actual = c.GetUsersList();

            Assert.AreEqual(0, actual.Count());

        }

        [Test]
        public void GetUsersList_GetFromUserList_ReturnsSameCollection()
        {
            var usersRepo = Substitute.For<IChatUserRepository>();
            var roomsRepo = Substitute.For<IChatRoomRepository>();

            var userList = new List<ChatUser>()
            {
                new ChatUser() { Id = 1 },
                new ChatUser() { Id = 2 },
            };

            usersRepo.GetAllUsers().Returns(userList);

            ChatController c = new ChatController(usersRepo, roomsRepo);

            var actual = c.GetUsersList();

            Assert.AreEqual(2, actual.Count());
            Assert.AreEqual(userList, actual);

        }

        #endregion

        #region GetChatRoomsList

        [Test]
        public void GetChatRoomsList_OnlyWithPublicChats_ReturnsPublicChatsOnly()
        {
            var usersRepo = Substitute.For<IChatUserRepository>();
            var roomsRepo = Substitute.For<IChatRoomRepository>();

            var dataChatUsers = new List<ChatUser>()
                {
                    new ChatUser { Id = 1, IsDeleted = false, Nick = "First"},
                };

            DateTime data = new DateTime(2017, 04, 22, 00, 00, 00, 0000);

            var dataChatRooms = new List<ChatRoom>
                {
                    new ChatRoom { Id = 1, IsPersonalChatRoom = false, LastChanged = data, Name = "Public"},
                };

            var dataChatUsersInRooms = new List<ChatUsersInRoom>();

            var currentUser = dataChatUsers[0];
            usersRepo.GetByAppUserId("").ReturnsForAnyArgs(currentUser);
            usersRepo.GetAllUsers().Returns(dataChatUsers);

            roomsRepo.GetAllPublicChatRooms().Returns(dataChatRooms);
            roomsRepo.GetAllPersonalChatRoomsForUserIncludingRooms(currentUser).Returns(dataChatUsersInRooms);

            ChatController c = new ChatController(usersRepo, roomsRepo);

            //Act
            var result = c.GetChatRoomsList();

            //Assert
            Assert.AreEqual(1, result.Count());
            var chat = result.First();
            Assert.AreEqual(1, chat.Id);
            Assert.AreEqual(false, chat.IsPersonalChatRoom);

            roomsRepo.DidNotReceiveWithAnyArgs().GetPartnerForPersonalChatRoom(dataChatRooms[0], dataChatUsers[0]);
        }

        [Test]
        public void GetChatRoomsList_OnlyWithPublicAndPrivateChats_ReturnsAllChats()
        {
            var usersRepo = Substitute.For<IChatUserRepository>();
            var roomsRepo = Substitute.For<IChatRoomRepository>();

            var dataChatUsers = new List<ChatUser>
                {
                    new ChatUser { Id = 1, IsDeleted = false, Nick = "First"},
                    new ChatUser { Id = 2, IsDeleted = false, Nick = "Second"},
                };

            DateTime data = new DateTime(2017, 04, 22, 00, 00, 00, 0000);

            var dataChatRoomsPub = new List<ChatRoom>
                {
                    new ChatRoom { Id = 1, IsPersonalChatRoom = false, LastChanged = data, Name = "Public"},
                };
            var dataChatRoomsPers = new List<ChatRoom>
                {
                    new ChatRoom { Id = 2, IsPersonalChatRoom = true,  LastChanged = data.AddMinutes(1), Name = "1+2" },
                };


            var dataChatUsersInRooms = new List<ChatUsersInRoom>
                {
                    new ChatUsersInRoom { Id = 4, ChatRoomId = 2, ChatUserId = 2, ChatRoom = dataChatRoomsPers[0], ChatUser= dataChatUsers[1] },
                };

            var currentUser = dataChatUsers[0];
            usersRepo.GetByAppUserId("").ReturnsForAnyArgs(currentUser);
            usersRepo.GetAllUsers().Returns(dataChatUsers);

            roomsRepo.GetAllPublicChatRooms().Returns(dataChatRoomsPub);
            roomsRepo.GetAllPersonalChatRoomsForUserIncludingRooms(currentUser).Returns(dataChatUsersInRooms);
            roomsRepo.GetPartnerForPersonalChatRoom(dataChatRoomsPers[0], dataChatUsers[0]).Returns(dataChatUsers[1]);

            ChatController c = new ChatController(usersRepo, roomsRepo);

            //Act
            var result = c.GetChatRoomsList();

            //Assert
            Assert.AreEqual(2, result.Count());
            var chat = result.First();
            Assert.AreEqual(1, chat.Id);
            Assert.AreEqual(false, chat.IsPersonalChatRoom);

            chat = result.Last();
            Assert.AreEqual(2, chat.Id);
            Assert.AreEqual(true, chat.IsPersonalChatRoom);
        }

        [Test]
        public void GetChatRoomsList_OnlyWithPublicAndPrivateChatsAndUsersWithoutChats_ReturnsAllChatsAndUsers()
        {
            var usersRepo = Substitute.For<IChatUserRepository>();
            var roomsRepo = Substitute.For<IChatRoomRepository>();

            var dataChatUsers = new List<ChatUser>
                {
                    new ChatUser { Id = 1, IsDeleted = false, Nick = "First"},
                    new ChatUser { Id = 2, IsDeleted = false, Nick = "Second"},
                    new ChatUser { Id = 3, IsDeleted = false, Nick = "Third"},
                };

            DateTime data = new DateTime(2017, 04, 22, 00, 00, 00, 0000);

            var dataChatRoomsPub = new List<ChatRoom>
                {
                    new ChatRoom { Id = 1, IsPersonalChatRoom = false, LastChanged = data, Name = "Public"},
                };

            var dataChatRoomsPers = new List<ChatRoom>
                {
                    new ChatRoom { Id = 2, IsPersonalChatRoom = true,  LastChanged = data.AddMinutes(1), Name = "1+2" },
                    new ChatRoom { Id = 3, IsPersonalChatRoom = true,  LastChanged = data.AddMinutes(2), Name = "1+3" },
                };

            var dataChatUsersInRooms = new List<ChatUsersInRoom>
                {
                    new ChatUsersInRoom { Id = 5, ChatRoomId = 2, ChatUserId = 2, ChatRoom = dataChatRoomsPers[0], ChatUser= dataChatUsers[1] },
                };

            var currentUser = dataChatUsers[0];

            usersRepo.GetByAppUserId("").ReturnsForAnyArgs(currentUser);
            usersRepo.GetAllUsers().Returns(dataChatUsers);

            roomsRepo.GetAllPublicChatRooms().Returns(dataChatRoomsPub);
            roomsRepo.GetAllPersonalChatRoomsForUserIncludingRooms(currentUser).Returns(dataChatUsersInRooms);
            roomsRepo.GetPartnerForPersonalChatRoom(dataChatRoomsPers[0], dataChatUsers[0]).Returns(dataChatUsers[1]);

            ChatController c = new ChatController(usersRepo, roomsRepo);

            //Act
            var result = c.GetChatRoomsList();

            //Assert
            Assert.AreEqual(3, result.Count());

            var chat = result.First();
            Assert.AreEqual(1, chat.Id);
            Assert.AreEqual(false, chat.IsPersonalChatRoom);

            chat = result.ElementAt(1);
            Assert.AreEqual(2, chat.Id);
            Assert.AreEqual(true, chat.IsPersonalChatRoom);

            chat = result.ElementAt(2);
            Assert.AreEqual(0, chat.Id);
            Assert.AreEqual(3, chat.ChatUserId);
            Assert.AreEqual(true, chat.IsPersonalChatRoom);
        }

        #endregion

    }
}
