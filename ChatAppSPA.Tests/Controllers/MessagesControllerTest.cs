﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

using NUnit.Framework;
using NSubstitute;

using ChatAppSPA.Controllers;
using ChatAppSPA.Models.Repositories;
using ChatAppSPA.Models.Entities;


namespace ChatAppSPA.Tests.Controllers
{
    [TestFixture]
    class MessagesControllerTest
    {
        #region GetMessages

        [Test]
        public void GetMessages_ForExistedChatWithNoMessgesAndGrantedAccess_ReturnsOkWithEmptyMessagesCollection()
        {
            var usersRepo = Substitute.For<IChatUserRepository>();
            var roomsRepo = Substitute.For<IChatRoomRepository>();
            var messagesRepo = Substitute.For<IMessageRepository>();

            var dataChatUsers = new List<ChatUser>
                {
                    new ChatUser { Id = 1, IsDeleted = false, Nick = "First"},
                    new ChatUser { Id = 2, IsDeleted = false, Nick = "Second"},
                    new ChatUser { Id = 3, IsDeleted = false, Nick = "Third"},
                };

            DateTime data = new DateTime(2017, 04, 22, 00, 00, 00, 0000);

            var dataChatRooms = new List<ChatRoom>
                {
                    new ChatRoom { Id = 1, IsPersonalChatRoom = false, LastChanged = data, Name = "Public"},
                    new ChatRoom { Id = 2, IsPersonalChatRoom = true,  LastChanged = data.AddMinutes(1), Name = "1+2" },
                    new ChatRoom { Id = 3, IsPersonalChatRoom = true,  LastChanged = data.AddMinutes(2), Name = "1+3" },
                };

            var dataMessages = new List<Message>();

            int from = 0;

            ChatUser currentUser = new ChatUser() { Id = 1, Nick = "1" };
            ChatRoom chatRoom = new ChatRoom() { Id = 1, Name = "1+1" };

            usersRepo.GetByAppUserIdAsync("").ReturnsForAnyArgs(currentUser);

            roomsRepo.GetChatRoomById(chatRoom.Id).Returns(chatRoom);
            roomsRepo.CanViewChatRoomData(chatRoom, currentUser).Returns(true);

            messagesRepo.GetMessagesForChatRoomAsync(chatRoom, from).Returns(Task.Run<IEnumerable<Message>>(() => { return dataMessages; }));


            MessagesController c = new MessagesController(usersRepo, roomsRepo, messagesRepo);

            //Act
            var t = c.GetMessages(chatRoom.Id, -1, from);
            t.Wait();

            var result = t.Result;

            //Assert
            Assert.IsInstanceOf(typeof(OkNegotiatedContentResult<IEnumerable<Message>>), result);

            var value = result as OkNegotiatedContentResult<IEnumerable<Message>>;
            Assert.AreEqual(0, value.Content.Count());
        }

        [Test]
        public void GetMessages_ForExistedChatWithMessgesAndGrantedAccess_ReturnsOkWithMessagesCollection()
        {
            var usersRepo = Substitute.For<IChatUserRepository>();
            var roomsRepo = Substitute.For<IChatRoomRepository>();
            var messagesRepo = Substitute.For<IMessageRepository>();

            var dataChatUsers = new List<ChatUser>
                {
                    new ChatUser { Id = 1, IsDeleted = false, Nick = "First"},
                    new ChatUser { Id = 2, IsDeleted = false, Nick = "Second"},
                    new ChatUser { Id = 3, IsDeleted = false, Nick = "Third"},
                };

            DateTime data = new DateTime(2017, 04, 22, 00, 00, 00, 0000);

            var dataChatRooms = new List<ChatRoom>
                {
                    new ChatRoom { Id = 1, IsPersonalChatRoom = false, LastChanged = data, Name = "Public"},
                    new ChatRoom { Id = 2, IsPersonalChatRoom = true,  LastChanged = data.AddMinutes(1), Name = "1+2" },
                    new ChatRoom { Id = 3, IsPersonalChatRoom = true,  LastChanged = data.AddMinutes(2), Name = "1+3" },
                };

            var dataMessages = new List<Message>
                {
                    new Message { Id = 1, ChatRoomId = 1, AuthorId = 1, ChatRoom = dataChatRooms[0], Author = dataChatUsers[0], Date = data.AddMinutes(1), Text = "1" },
                    new Message { Id = 2, ChatRoomId = 1, AuthorId = 2, ChatRoom = dataChatRooms[0], Author = dataChatUsers[1], Date = data.AddMinutes(2), Text = "2" },
                    new Message { Id = 3, ChatRoomId = 1, AuthorId = 3, ChatRoom = dataChatRooms[0], Author = dataChatUsers[2], Date = data.AddMinutes(3), Text = "3" },
                    new Message { Id = 4, ChatRoomId = 1, AuthorId = 1, ChatRoom = dataChatRooms[0], Author = dataChatUsers[0], Date = data.AddMinutes(4), Text = "4" },
                };

            MessagesController c = new MessagesController(usersRepo, roomsRepo, messagesRepo);

            int from = 0;

            ChatUser currentUser = new ChatUser() { Id = 1, Nick = "1" };
            ChatRoom chatRoom = new ChatRoom() { Id = 1, Name = "1+1" };

            usersRepo.GetByAppUserIdAsync("").ReturnsForAnyArgs(currentUser);

            roomsRepo.GetChatRoomById(chatRoom.Id).Returns(chatRoom);
            roomsRepo.CanViewChatRoomData(chatRoom, currentUser).Returns(true);

            messagesRepo.GetMessagesForChatRoomAsync(chatRoom, from).Returns(Task.Run<IEnumerable<Message>>(() => { return dataMessages; }));

            //Act
            var t = c.GetMessages(chatRoom.Id, -1, from);
            t.Wait();

            var result = t.Result;

            //Assert
            Assert.IsInstanceOf(typeof(OkNegotiatedContentResult<IEnumerable<Message>>), result);

            var value = result as OkNegotiatedContentResult<IEnumerable<Message>>;
            Assert.AreEqual(4, value.Content.Count());

            var msg = value.Content.First();
            Assert.AreEqual(1, msg.Id);

            msg = value.Content.Last();
            Assert.AreEqual(4, msg.Id);
        }

        [Test]
        public void GetMessages_ForNonExistedChat_ReturnsNotFound()
        {
            var usersRepo = Substitute.For<IChatUserRepository>();
            var roomsRepo = Substitute.For<IChatRoomRepository>();
            var messagesRepo = Substitute.For<IMessageRepository>();

            var dataChatUsers = new List<ChatUser>
                {
                    new ChatUser { Id = 1, IsDeleted = false, Nick = "First"},
                    new ChatUser { Id = 2, IsDeleted = false, Nick = "Second"},
                    new ChatUser { Id = 3, IsDeleted = false, Nick = "Third"},
                };

            DateTime data = new DateTime(2017, 04, 22, 00, 00, 00, 0000);

            var dataChatRooms = new List<ChatRoom>
                {
                    new ChatRoom { Id = 1, IsPersonalChatRoom = false, LastChanged = data, Name = "Public"},
                    new ChatRoom { Id = 2, IsPersonalChatRoom = true,  LastChanged = data.AddMinutes(1), Name = "1+2" },
                    new ChatRoom { Id = 3, IsPersonalChatRoom = true,  LastChanged = data.AddMinutes(2), Name = "1+3" },
                };

            var dataMessages = new List<Message>
                {
                    new Message { Id = 1, ChatRoomId = 1, AuthorId = 1, ChatRoom = dataChatRooms[0], Author = dataChatUsers[0], Date = data.AddMinutes(1), Text = "1" },
                    new Message { Id = 2, ChatRoomId = 1, AuthorId = 2, ChatRoom = dataChatRooms[0], Author = dataChatUsers[1], Date = data.AddMinutes(2), Text = "2" },
                    new Message { Id = 3, ChatRoomId = 1, AuthorId = 3, ChatRoom = dataChatRooms[0], Author = dataChatUsers[2], Date = data.AddMinutes(3), Text = "3" },
                    new Message { Id = 4, ChatRoomId = 1, AuthorId = 1, ChatRoom = dataChatRooms[0], Author = dataChatUsers[0], Date = data.AddMinutes(4), Text = "4" },
                };

            MessagesController c = new MessagesController(usersRepo, roomsRepo, messagesRepo);

            int from = 0;
            ChatUser currentUser = new ChatUser() { Id = 1, Nick = "1" };
            ChatRoom chatRoom = new ChatRoom() { Id = 235, Name = "1+235" };

            usersRepo.GetByAppUserIdAsync("").ReturnsForAnyArgs(currentUser);

            roomsRepo.GetChatRoomById(chatRoom.Id).Returns<ChatRoom>((ChatRoom)null);
            roomsRepo.CanViewChatRoomData(chatRoom, currentUser).Returns(true);

            messagesRepo.GetMessagesForChatRoomAsync(chatRoom, from).Returns(Task.Run<IEnumerable<Message>>(() => { return dataMessages; }));

            //Act
            var t = c.GetMessages(chatRoom.Id, -1, from);
            t.Wait();

            var result = t.Result;

            //Assert
            Assert.IsInstanceOf(typeof(NotFoundResult), result);
        }

        [Test]
        public void GetMessages_ForExistedChatWithoutGrantedAccess_ReturnsBadRequest()
        {
            var usersRepo = Substitute.For<IChatUserRepository>();
            var roomsRepo = Substitute.For<IChatRoomRepository>();
            var messagesRepo = Substitute.For<IMessageRepository>();

            var dataChatUsers = new List<ChatUser>
                {
                    new ChatUser { Id = 1, IsDeleted = false, Nick = "First"},
                    new ChatUser { Id = 2, IsDeleted = false, Nick = "Second"},
                    new ChatUser { Id = 3, IsDeleted = false, Nick = "Third"},
                };

            DateTime data = new DateTime(2017, 04, 22, 00, 00, 00, 0000);

            var dataChatRooms = new List<ChatRoom>
                {
                    new ChatRoom { Id = 1, IsPersonalChatRoom = false, LastChanged = data, Name = "Public"},
                    new ChatRoom { Id = 2, IsPersonalChatRoom = true,  LastChanged = data.AddMinutes(1), Name = "1+2" },
                    new ChatRoom { Id = 3, IsPersonalChatRoom = true,  LastChanged = data.AddMinutes(2), Name = "1+3" },
                };

            var dataMessages = new List<Message>
                {
                    new Message { Id = 1, ChatRoomId = 1, AuthorId = 1, ChatRoom = dataChatRooms[0], Author = dataChatUsers[0], Date = data.AddMinutes(1), Text = "1" },
                    new Message { Id = 2, ChatRoomId = 1, AuthorId = 2, ChatRoom = dataChatRooms[0], Author = dataChatUsers[1], Date = data.AddMinutes(2), Text = "2" },
                    new Message { Id = 3, ChatRoomId = 1, AuthorId = 3, ChatRoom = dataChatRooms[0], Author = dataChatUsers[2], Date = data.AddMinutes(3), Text = "3" },
                    new Message { Id = 4, ChatRoomId = 1, AuthorId = 1, ChatRoom = dataChatRooms[0], Author = dataChatUsers[0], Date = data.AddMinutes(4), Text = "4" },
                };

            MessagesController c = new MessagesController(usersRepo, roomsRepo, messagesRepo);

            int from = 0;
            ChatUser currentUser = new ChatUser() { Id = 1, Nick = "1" };
            ChatRoom chatRoom = new ChatRoom() { Id = 1, Name = "1+1" };

            usersRepo.GetByAppUserIdAsync("").ReturnsForAnyArgs(currentUser);

            roomsRepo.GetChatRoomById(chatRoom.Id).Returns(chatRoom);
            roomsRepo.CanViewChatRoomData(chatRoom, currentUser).Returns(false);

            messagesRepo.GetMessagesForChatRoomAsync(chatRoom, from).Returns(Task.Run<IEnumerable<Message>>(() => { return dataMessages; }));

            //Act
            var t = c.GetMessages(chatRoom.Id, -1, from);
            t.Wait();

            var result = t.Result;

            //Assert
            Assert.IsInstanceOf(typeof(BadRequestErrorMessageResult), result);
        }

        #endregion

    }
}
